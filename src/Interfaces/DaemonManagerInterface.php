<?php

namespace Vladimirgs\LaravelDaemon\Interfaces;

interface DaemonManagerInterface
{
    /**
     * Determine if the daemon workers should restart.
     *
     * @return bool
     */
    public function shouldRestart(): bool;

    /**
     * Update the cache entry to restart the daemon.
     *
     * @return void
     */
    public function restart(): void;

    /**
     * Get the time of the last restart.
     *
     * @return string
     */
    public function getLastRestart(): string;
}
