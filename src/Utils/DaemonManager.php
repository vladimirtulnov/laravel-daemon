<?php

namespace Vladimirgs\LaravelDaemon\Utils;

use Illuminate\Contracts\Cache\Repository as Cache;
use Illuminate\Support\InteractsWithTime;
use Vladimirgs\LaravelDaemon\Interfaces\DaemonManagerInterface;

class DaemonManager implements DaemonManagerInterface
{
    use InteractsWithTime;

    /**
     * The cache store implementation.
     *
     * @var \Illuminate\Contracts\Cache\Repository
     */
    protected $cache;

    /**
     * The last restart entry in the cache.
     *
     * @var string
     */
    protected string $lastRestart = '';

    public function __construct(Cache $cache)
    {
        $this->cache = $cache;

        $this->lastRestart = $this->getLastRestart();
    }

    /**
     * Determine if the daemon workers should restart.
     *
     * @return bool
     */
    public function shouldRestart(): bool
    {
        return $this->lastRestart !== $this->getLastRestart();
    }

    /**
     * Update the cache entry to restart the daemons.
     *
     * @return void
     */
    public function restart(): void
    {
        $this->lastRestart = $this->currentTime();

        $this->cache->forever($this->getCacheKey(), $this->lastRestart);
    }

    /**
     * Get the time of the last restart.
     *
     * @return string
     */
    public function getLastRestart(): string
    {
        return (string) $this->cache->get($this->getCacheKey(), null);
    }

    /**
     * Return the cache key.
     *
     * @return string
     */
    private function getCacheKey(): string
    {
        return config('daemon.keys.restart');
    }
}
