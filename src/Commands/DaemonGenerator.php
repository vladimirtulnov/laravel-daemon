<?php

namespace Vladimirgs\LaravelDaemon\Commands;

use Illuminate\Console\GeneratorCommand;
use Symfony\Component\Console\Input\InputArgument;

class DaemonGenerator extends GeneratorCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'make:daemon';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a new daemon command';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Daemon';

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return file_exists(resource_path('stubs/Daemon.stub'))
            ? resource_path('stubs/Metrics/Daemon.stub')
            : __DIR__ . '/../Stubs/Daemon.stub';
    }

    /**
     * Get the default namespace for the class.
     *
     * @param string $rootNamespace
     *
     * @return string
     */
    protected function getDefaultNamespace($rootNamespace)
    {
        return $rootNamespace . '\\Console\\Commands\\Daemons';
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['name', InputArgument::REQUIRED, 'The name of the action.'],
        ];
    }
}
