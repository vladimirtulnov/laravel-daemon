<?php

namespace Vladimirgs\LaravelDaemon\Commands;

use Illuminate\Console\Command;
use Vladimirgs\LaravelDaemon\Interfaces\DaemonManagerInterface;

class RestartDaemon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daemon:restart';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restart all running daemons.';

    protected DaemonManagerInterface $manager;

    /**
     * Create a new queue restart command.
     *
     * @param  \VoodooSMS\LaravelDaemon\Interfaces\DaemonManagerInterface
     * @return void
     */
    public function __construct(DaemonManagerInterface $manager)
    {
        parent::__construct();

        $this->manager = $manager;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->manager->restart();

        $this->info('Broadcasting the daemon restart signal.');
    }
}
