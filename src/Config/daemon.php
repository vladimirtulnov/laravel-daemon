<?php

return [
    /**
     * The number of seconds between iterations of the worker.
     *
     * Note: this can be a fraction, e.g. 0.25
     */
    'sleep' => (float) env('DAEMON_SLEEP', 1.0),

    /**
     * A list of keys used in the cache to control daemon workers.
     * You should change these if you have multiple projects
     * running daemons using the same cache repository.
     */
    'keys' => [
        'restart' => 'voodoosms:daemon:restart',
    ],
];
