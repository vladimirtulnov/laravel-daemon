# Laravel Daemon

This package allows you to build long-running commands/daemons. This can be useful when you need something to run with higher frequeny than the laravel schedule (i.e. more frequent than 1 minute).

## Installation

```shell
composer require vladimirgs/laravel-daemon
```

## Creating a Daemon

To create a daemon, run:

```shell
php artisan make:daemon
```

This will create a new file in `app/Console/Commands/Daemons`. The `Daemon` abstract extends a normal laravel command, so the underlying methods in a normal laravel command are still available. 

### Adding functionality

The only difference between a daemon and a normal command, is that the logic now goes into a `work(): void` method instead of the `handle()` method.

E.g. a basic example that just prints `hello` to the console:

```php
public function work(): void
{
    $this->info('Hello');
}
```

You can get the current iteration number by accessing `$this->runs`.

You can use Laravel's dependency injection by adding parameters to the work method.

### Set up

You can use the `setUp(): void` method to perform some operations before the daemon starts, such as parsing arguments and options that have been passed to the command. This is similar to the `setUp` method used in PHPUnit test cases.

```php
public function setUp(): void
{
    //
}
```

### Tear down

You can use the `tearDown(): void` method to perform some operations after the daemon finishes, this is similar to the `tearDown` method used in PHPUnit test cases.

```php
public function tearDown(): void
{
    //
}
```

### Output

The output from the above example is:

```shell
[2021-08-06 13:45:26.140] - V5wkJjo - Starting daemon
[2021-08-06 13:45:26.140] - V5wkJjo:a8c17bd - Hello
[2021-08-06 13:45:27.141] - V5wkJjo:7f48899 - Hello
[2021-08-06 13:45:28.141] - V5wkJjo:07e9cbd - Hello
[2021-08-06 13:45:29.142] - V5wkJjo:e69bc62 - Hello
```

You'll notice that all output gets prepended with a timestamp and seemingly random characters. The random characters are instance and batch ids. This is useful for when you run multiple instances of the same daemon and need to filter out the logs. You can access both of these values using the `getInstanceId()` and `getBatchId()` methods.

## Options

### Running the loop once

Sometimes you might only want to run the loop once, to do this you can add the `--once` flag when running the command.

Note: this takes precedence over the `--until-complete` flag.

### Running until complete

Similarly, you may want the daemon to keep running until the work it is doing is finished, you can do this by using the `--until-complete` flag. For this to work, you need to call `$this->completed()` method at some point in the `work()` method.

### Restarting Daemons

Depending on how you host your project, you may need to restart daemon workers when new code is deployed. You can do this by running: `php artisan daemon:restart`. This works the same way that `php artisan queue:restart` works i.e. the process exits - you should use a process manager which will automatically restart the daemon workers.

### Configuration

#### Sleep

The daemon will pause after every iteration before continuing. The default is a 1 second pause, but this can be overridden using the `DAEMON_SLEEP` environment variable. This takes units in seconds and is a float, e.g. `0.1`.

Alternatively, you can call the daemon with a flag to override this at runtime: `--sleep=0.1`.

